<?php

namespace Database\Factories;

use App\Models\User;
use Illuminate\Database\Eloquent\Factories\Factory;
use Illuminate\Support\Str;

class UserFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = User::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            'name' => $this->faker->name(),
            'email' => $this->faker->unique()->safeEmail(),
            'photo_url' => $this->faker->randomElement(['https://bucket-for-test-blog.s3.us-east-2.amazonaws.com/User27photo.png',
                                                        'https://bucket-for-test-blog.s3.us-east-2.amazonaws.com/User34photo.png']),
            'password' => bcrypt('FakePas') // password
        ];
    }
}
