<?php

namespace Database\Factories;

use App\Models\Product;
use App\Models\User;
use Illuminate\Database\Eloquent\Factories\Factory;

class ProductFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = Product::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            'user_id' => $this->faker->randomElement(User::all())->id,
            'name' => $this->faker->realText(15),
            'description' => $this->faker->realText(100),
            'photo_url' => $this->faker->randomElement(['https://bucket-for-test-blog.s3.us-east-2.amazonaws.com/Product11photo.jpeg',
                'https://bucket-for-test-blog.s3.us-east-2.amazonaws.com/Product12photo.jpeg']),
            'price' => $this->faker->randomNumber(2)
        ];
    }
}
