<?php

namespace Database\Seeders;

use App\Models\Blog;
use App\Models\Category;
use App\Models\Product;
use App\Models\User;
use Illuminate\Database\Seeder;

class ProductsSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $blogs = Product::factory(8)->create()->each(function ($product){
            $product->media()->create(["media_path" => substr($product->photo_url, -15)]);
            $product->save();
        });
    }
}
