<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Spatie\Permission\Models\Permission;
use Spatie\Permission\Models\Role;

class RolesAndPermissionsSeeder extends Seeder
{
    /**
     * all permissions
     */
    protected const PERMISSIONS = [
        'listProducts' => 'product-list',
        'viewProduct' => 'product-view',
        'createProduct' => 'product-create',
        'editProduct' => 'product-edit',
        'deleteProduct' => 'product-delete',
    ];


    /**
     * permissions by role
     */
    protected const ROLE_PERMISSIONS = [
        'admin' => [self::PERMISSIONS],
        'user' => [
            self::PERMISSIONS['listProducts'],
            self::PERMISSIONS['viewProduct'],
        ]
    ];


    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        foreach (self::PERMISSIONS as $permission) {
            Permission::updateOrCreate(['name' => $permission]);
        }

        foreach (self::ROLE_PERMISSIONS as $role => $permissions) {
            $createdRole = Role::updateOrCreate(['name' => $role, 'guard_name' => 'api']);
            foreach ($permissions as $permission) {
                $createdRole->givePermissionTo($permission);
            }
        }
    }
}
