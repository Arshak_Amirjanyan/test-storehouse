<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\HasOne;
use Illuminate\Database\Eloquent\Relations\MorphOne;

class Product extends Model
{
    use HasFactory;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name',
        'user_id',
        'description',
        'photo_url',
        'price'
    ];

    protected $hidden = ["pivot"];


    /**
     * @return MorphOne
     */
    public function media(): MorphOne
    {
        return $this->morphOne(Media::class, 'mediaholder');
    }

    /**
     * @return HasOne
     */
    public function user(): HasOne
    {
        return $this->hasOne(Product::class, "user_id");
    }

    /**
     * Used as a filter in ProductRepository
     *
     * @param Builder $query
     * @param int $user_id
     * @return Builder
     */
    public function scopeByUser(Builder $query, int $user_id): Builder
    {
        return $query->where("user_id", $user_id);
    }

    public function scopeByName(Builder $query, string $name)
    {
        return $query->where("name", "LIKE", "%" . $name . "%");
    }

    /**
     * @param Builder $query
     * @param float $price
     * @return Builder
     */
    public function scopeByPriceFrom(Builder $query, float $price): Builder
    {
        return $query->where("price", ">", $price);
    }

    /**
     * @param Builder $query
     * @param float $price
     * @return Builder
     */
    public function scopeByPriceTo(Builder $query, float $price): Builder
    {
        return $query->where("price", "<", $price);
    }

    /**
     * @param Builder $query
     * @param $start_date
     * @return Builder
     */
    public function scopeByStartDate(Builder $query, $start_date): Builder
    {
        return $query->where("created_at", ">", $start_date);
    }

    /**
     * @param Builder $query
     * @param $start_date
     * @return Builder
     */
    public function scopeByEndDate(Builder $query, $start_date): Builder
    {
        return $query->where("created_at", "<", $start_date);
    }
}
