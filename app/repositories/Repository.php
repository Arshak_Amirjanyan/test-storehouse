<?php

namespace App\repositories;

use App\Models\User;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;

abstract class Repository
{
    /**
     * @var Model
     */
    protected Model $model;

    /**
     * @param Model $model
     */
    public function __construct(Model $model)
    {
        $this->model = $model;
    }

    /**
     * @param array $filters
     * @return Builder
     */
    public function applyFilters(array $filters = []): Builder
    {
        $query = $this->model->newQuery();
        foreach ($filters as $filter => $value) {
            $filter = $this->beautifyFilter($filter);
            $query->mergeConstraintsFrom($this->model::$filter($value));
        }

        return $query;
    }

    public function beautifyFilter($filter): string
    {
        $filter = "by" . $filter;
        $filter = str_replace('_', '', $filter);

        return $filter;
    }
}
