<?php

namespace App\repositories;

use App\Models\Product;
use Illuminate\Database\Eloquent\Model;

class ProductRepository extends Repository
{
    /**
     * @param Product $model
     */
    public function __construct(Product $model)
    {
        parent::__construct($model);
    }
}
