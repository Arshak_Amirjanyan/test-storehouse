<?php

namespace App\Http\Requests\Product;

use App\Models\Product;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Support\Facades\Auth;

class ListProductsRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize(): bool
    {
        return Auth::user()->can("viewAny", Product::class);
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules(): array
    {
        return [
            "name" => "sometimes|string",
            "priceFrom" => "sometimes|numeric",
            "priceTo" => "sometimes|numeric",
            'start_date'  =>  'sometimes|date',
            'end_date'    =>  'sometimes|date|after_or_equal:start_date'
        ];
    }
}
