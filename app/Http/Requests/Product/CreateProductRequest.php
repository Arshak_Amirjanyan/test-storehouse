<?php

namespace App\Http\Requests\Product;

use App\Models\Product;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Support\Facades\Auth;

class CreateProductRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize(): bool
    {
        return Auth::user()->can('create', Product::class);
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules(): array
    {
        return [
            'name' => 'required|string',
            'description' => 'sometimes|string|min:20|max:200',
            'price' => "required|numeric",
            'photo' => "string|max:3145728000"  //4000 * 1024 *1024 *3/4 to get the size in bytes. checks for image size < 4000 MB
        ];
    }

    /**
     * @return string[]
     */
    public function messages(): array
    {
        return [
            'name.required' => 'A name is required',
            'description.string' => 'A description must be a string',
            'description.min' => 'A description must be longer than 20 characters',
            'description.max' => 'A description must be shorter than 200 characters',
            'price.required' => 'A price is required',
            'price.number' => 'The price must be a number',
        ];
    }
}
