<?php

namespace App\Http\Controllers;

use App\Http\Requests\Product\CreateProductRequest;
use App\Http\Requests\Product\DeleteProductRequest;
use App\Http\Requests\Product\ListProductsRequest;
use App\Http\Requests\Product\ShowProductRequest;
use App\Http\Requests\Product\UpdateProductRequest;
use App\Http\Resources\Product\ProductResource;
use App\Http\Resources\Product\ProductsCollection;
use App\Http\Responses\ApiResponse;
use App\Models\Product;
use App\repositories\ProductRepository;
use App\Services\ImagableService;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

class ProductController extends Controller
{
    /**
     * @var ImagableService
     */
    protected ImagableService $imagableService;

    /**
     * @var ProductRepository
     */
    protected ProductRepository $productRepository;

    /**
     * @param ImagableService $imagableService
     * @param ProductRepository $productRepository
     */
    public function __construct(ImagableService $imagableService, ProductRepository $productRepository)
    {
        $this->imagableService = $imagableService;
        $this->productRepository = $productRepository;
    }

    /**
     * @param ListProductsRequest $request
     * @return ApiResponse
     */
    public function index(ListProductsRequest $request): ApiResponse
    {
        $products = $this->productRepository->applyFilters($request->validated())->get();

        return new ApiResponse(ProductsCollection::make($products), 200);
    }

    /**
     * @param Product $product
     * @return ApiResponse
     */
    public function show(ShowProductRequest $request, Product $product): ApiResponse
    {
        return new ApiResponse(ProductResource::make($product));
    }

    /**
     * @param CreateProductRequest $request
     * @return ApiResponse
     */
    public function store(CreateProductRequest $request): ApiResponse
    {
        $data = $request->validated();
        $data["user_id"] = Auth::user()->id;
        $product = Product::create($data);
        if ($imageBase64 = $request->get('photo')) {
            $this->imagableService->handleImage($product, $imageBase64);
        }
        return new ApiResponse(ProductResource::make($product));
    }

    /**
     * @param Product $product
     * @param UpdateProductRequest $request
     * @return ApiResponse
     */
    public function update(Product $product, UpdateProductRequest $request): ApiResponse
    {
        $product->update($request->all());
        if ($imageBase64 = $request->get('photo')) {
            $this->imagableService->handleImage($product, $imageBase64);
        }
        return new ApiResponse(ProductResource::make($product));
    }

    /**
     * @param Product $product
     * @param DeleteProductRequest $request
     * @return ApiResponse
     */
    public function destroy(Product $product, DeleteProductRequest $request): ApiResponse
    {
        if ($product->photo_url) {
            $this->imagableService->handleDelete($product);
        }
        $product->delete();

        return new ApiResponse(ProductResource::make($product));
    }

}
