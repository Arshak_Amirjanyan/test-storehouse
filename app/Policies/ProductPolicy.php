<?php

namespace App\Policies;

use App\Models\Product;
use App\Models\User;
use Illuminate\Auth\Access\HandlesAuthorization;

class ProductPolicy
{
    use HandlesAuthorization;

    /**
     * @param User $user
     * @return bool
     */
    public function viewAny(User $user): bool
    {
        return $user->can('product-list');
    }

    /**
     * @param User $user
     * @param Product $product
     * @return bool
     */
    public function view(User $user, Product $product): bool
    {
        return $user->can('product-view');
    }

    /**
     * @param User $user
     * @return bool
     */
    public function create(User $user): bool
    {
        return $user->can('product-create');
    }

    /**
     * @param User $user
     * @param Product $product
     * @return bool
     */
    public function update(User $user, Product $product): bool
    {
        return $user->can('product-edit');
    }

    /**
     * @param User $user
     * @param Product $product
     * @return bool
     */
    public function delete(User $user, Product $product): bool
    {
        return $user->can('product-delete') ;
    }

    /**
     * @param User $user
     * @param Product $product
     * @return void
     */
    public function restore(User $user, Product $product)
    {
        //
    }

    /**
     * @param User $user
     * @param Product $product
     * @return void
     */
    public function forceDelete(User $user, Product $product)
    {
        //
    }
}
