<?php

namespace App\Exceptions;

use Exception;
use Symfony\Component\HttpFoundation\Exception\RequestExceptionInterface;

class BadRequestException extends \UnexpectedValueException implements RequestExceptionInterface
{
    //
}
